//
//  HomeViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 27/11/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit
import CoreData


class HomeViewController: UIViewController {

    //To get all the product details
    var dataController = DataController.sharedInstance


    @IBOutlet weak var homeGridView: UICollectionView!
    @IBOutlet weak var birthDetailsButton: UIButton!
    @IBOutlet weak var rotatingEarth: UIImageView!
    
    @IBOutlet weak var loaderContainer: UIView!
    @IBOutlet weak var loaderText: UILabel!
    @IBOutlet weak var loaderText1: UILabel!
    @IBOutlet weak var loaderText2: UILabel!
    
    //  array of collection view cells
    var cells: [UICollectionViewCell] = []
    var cellTitle: String!
    var cellText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(dataController.birthDate != nil) {
            setButtonDate()
        }
    }
    
    override func viewDidAppear(stat: Bool) {
        super.viewDidAppear(stat)
        //Check if data is already saved
        if(dataController.homePageCells.count > 0){
            // if there are values in cell then dont call server to fetch values
            print("have home page cells do nothing")
            setButtonDate()
            //Check if date has changed after loading data
            if(dataController.dateChanged == true) {
                print("Date has changed. Getting data again.")
                dataController.homePageCells = []
                self.homeGridView.reloadData()
                dataController.dateChanged = false
                //Start the loader and get details from server
                getDetails()
            }
        }
        else {
            print("Dont have home page cells")
            print("Birth date got is \(dataController.birthDate)")
            if (dataController.birthDate != nil && dataController.birthTime != nil && dataController.birthPlace != nil) {
                dataController.dateChanged = false
                //Start the loader and get details from server
                getDetails()
            }
            else {
                print("Show alert for Skip button")
                
                let alert = UIAlertController(title: "Birth Details Missing", message: "Please input all birth details for personalised experience.", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Cancel,handler: nil))
                
                alert.addAction(UIAlertAction(title: "Enter Birth Details", style: UIAlertActionStyle.Default,
                    handler:  {
                        action in
                        self.performSegueWithIdentifier("goToBirthDetails", sender: self)
                    }
                    )
                )
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //To set the view of button with current date
    func setButtonDate() {
        let birthDateNew = convertDateFormat(dataController.birthDate)
        birthDetailsButton.setTitle("\(birthDateNew) \(dataController.birthTime)", forState: .Normal)
    }
    
    func rotateEarth() {
        self.rotatingEarth.animationImages = []
        
        //Add image names to the array
        for(var x=0; x<44; x++) {
            self.rotatingEarth.animationImages?.append(UIImage(named: "tmp-\(x).gif")!)
        }
        self.rotatingEarth.animationDuration = 4.0
        self.rotatingEarth.startAnimating()
    }
    
    func changeLoaderText() {
        let viewWidth = view.bounds.width
        self.loaderText1.center.x -= viewWidth
        self.loaderText2.center.x -= viewWidth
        self.loaderText.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        self.loaderText1.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        self.loaderText2.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        
        UIView.animateKeyframesWithDuration(20, delay: 0, options: .Repeat, animations: {
            UIView.addKeyframeWithRelativeStartTime(0.175, relativeDuration: 0.1) {
                self.loaderText.center.x -= viewWidth
                self.loaderText1.center.x += viewWidth
            }
            UIView.addKeyframeWithRelativeStartTime(0.45, relativeDuration: 0.1) {
                self.loaderText1.center.x -= viewWidth
                self.loaderText2.center.x += viewWidth
            }
            UIView.addKeyframeWithRelativeStartTime(0.725, relativeDuration: 0.1) {
                self.loaderText2.center.x -= viewWidth
                self.loaderText.center.x += viewWidth
            }
        }, completion: nil)
    }
    
    //Start the loader and get details from server
    func getDetails() {
        //Start loader animation
        self.loaderContainer.hidden = false
        rotateEarth()
        changeLoaderText()
        
        //To set the view of button with current date
        setButtonDate()
        
        dataController.getHomePageCells(
            {
                (stat) in
                if (stat){
                    print (" got response in home view controller... reloading data now.")
                    //Reload the Home Grid view
                    dispatch_async(dispatch_get_main_queue(), {
                        self.homeGridView.reloadData()
                    })
                    
                }
                else {
                    print("Show alert for network problem")
                    
                    let alert = UIAlertController(title: "Network Error", message: "Please try again as there was some network error.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alert.addAction(UIAlertAction(title: "Re-Start", style: UIAlertActionStyle.Default,
                        handler:  {
                            action in
                            self.performSegueWithIdentifier("homeToBirthDetails", sender: self)
                        }
                        )
                    )
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                // stop animation
                self.loaderContainer.hidden = true
                self.rotatingEarth.stopAnimating()
            }
        )

    }
    
    
    //Set no of grids
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataController.homePageCells.count
    }
    
    let bounds = UIScreen.mainScreen().bounds
    
    //To set the size of cell according to screen
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        
        let screenWidth = bounds.width
        
        let margin:CGFloat = 5
        let aspectRatio:CGFloat = 170/250 //      Aspect ratio: 170/250 (w/h)
        
//        print("Screen Width is \(screenWidth)")
        
        if(dataController.homePageCells.count%2 != 0){
            // odd entries. make last entry long
            if(dataController.homePageCells.count == (indexPath.item + 1)){
                
                return CGSizeMake((screenWidth - margin*2), (((screenWidth - (margin*3))/(2*aspectRatio))))
                
            }
        }
        
        return CGSizeMake((screenWidth - (margin*3))/2, (((screenWidth - (margin*3))/(2*aspectRatio))))
        
    }
    
    
    //Set button of each category
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let homeGridCell = collectionView.dequeueReusableCellWithReuseIdentifier("homeGridCells", forIndexPath: indexPath)
        
        let homeCellImage: UIImageView = homeGridCell.viewWithTag(3) as! UIImageView

        let homeCellLabel: UILabel = homeGridCell.viewWithTag(11) as! UILabel

        let homeCellText: UILabel = homeGridCell.viewWithTag(2) as! UILabel
        
        //Clean the title and then use it
        cellTitle = dataController.homePageCells[indexPath.row]["title"] as! String
        //homeCellLabel.text = dataController.homeCellTitleNames[catName!]
        cellTitle = removeHtmlTags(cellTitle)
        cellTitle = String(cellTitle.characters.prefix(1)).uppercaseString + String(cellTitle.characters.dropFirst())
        homeCellLabel.text = cellTitle
        
        //Remove html tags and underscore
        let htmlText = dataController.homePageCells[indexPath.row]["text"] as! String
        cellText = removeHtmlTags(htmlText)
        homeCellText.text = cellText
        
        homeCellImage.image = UIImage(named: dataController.homePageCells[indexPath.row]["image"] as! String)

        //Set shadow for collection view cell
        homeGridCell.layer.shadowColor = UIColor(white: 0x000000, alpha: 0.5).CGColor
        homeGridCell.layer.shadowOffset = CGSizeMake(0, 1)
        homeGridCell.layer.shadowOpacity = 1.0
        homeGridCell.layer.shadowRadius = 0.3
        homeGridCell.layer.masksToBounds = false

        cells.append(homeGridCell)
        
        return homeGridCell
        
    }
    
    var selectedRow = Int()
    
    // onclick event of collection view
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        print("Cell Selected is \(indexPath)")
        
        // Let's assume that the segue name is called playerSegue
        // This will perform the segue and pre-load the variable for you to use
        
        self.selectedRow = indexPath.row

//      collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.Top)
        
        self.performSegueWithIdentifier("goToDetailsPage", sender: self)
        
    }
    
    //Function to find month name
    func convertDateFormat(birthDate: String) -> String {
        var dateArr = birthDate.characters.split("-")
        var monthName: String
        let monthNo = String(dateArr[1])
        print("Month no is \(monthNo)")
        switch (monthNo) {
            case "01":
                monthName = "January"
            case "02":
                monthName = "February"
            case "03":
                monthName = "March"
            case "04":
                monthName = "April"
            case "05":
                monthName = "May"
            case "06":
                monthName = "June"
            case "07":
                monthName = "July"
            case "08":
                monthName = "August"
            case "09":
                monthName = "September"
            case "10":
                monthName = "October"
            case "11":
                monthName = "Novembor"
            case "12":
                monthName = "December"
            default:
                monthName = "Invalid"
        }
        print("Month Name is \(monthName)")
        let newDate = "\(String(dateArr[0])) \(monthName) \(String(dateArr[2]))"
        print("New date is \(newDate)")
        return newDate
    }
    
    //Function to edit birth details
    @IBAction func editBirthDetails(sender: AnyObject) {
        self.performSegueWithIdentifier("goToBirthDetails", sender: self)
    }


    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    //To open new views
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        //        let nav = segue.destinationViewController as!
        //        nav.selectedProduct = selectedProduct
        //        nav.selectedCategory = selectedCategory
        //        nav.previousPage = "category"
        
        if(segue.identifier == "goToBirthDetails"){
            print("Prepare to segue into Birth Details Page")
        }else if(segue.identifier == "goToDetailsPage"){
            
            print ("Going To Details Page with Selected Item as : \(self.selectedRow)")
            
            let detailsToShow = segue.destinationViewController as! DetailViewController
            
            detailsToShow.cameFrom = 1
            detailsToShow.mainImageName = dataController.homePageCells[self.selectedRow]["image"] as! String
            
            let htmlText = dataController.homePageCells[self.selectedRow]["text"] as! String
            cellText = removeHtmlTags(htmlText)
            
            let title = dataController.homePageCells[self.selectedRow]["title"] as! String
            cellTitle = removeHtmlTags(title)
            cellTitle = String(cellTitle.characters.prefix(1)).uppercaseString + String(cellTitle.characters.dropFirst())
            
            detailsToShow.mainNameText = cellTitle
            detailsToShow.mainTextText = cellText

        }
    }
    
    //Remove html tags and underscore
    func removeHtmlTags(htmlText: String) -> String {
        var convertedText = htmlText.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil)
        convertedText = convertedText.stringByReplacingOccurrencesOfString("_", withString: " ", options: .RegularExpressionSearch, range: nil)
        //Remove more than one new line with double new line
        convertedText = convertedText.stringByReplacingOccurrencesOfString("[\r\\n]{4,}", withString: "\n\n", options: .RegularExpressionSearch, range: nil)
        //Remove extra space and new line in the beginning and end
        convertedText = convertedText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        return convertedText
    }
    
    //Change status bar text to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
}
